import java.awt.*;

public class Vector3 {
    public static Vector3 zero() {
        return new Vector3(0, 0, 0);
    }

    public static Vector3 one() {
        return new Vector3(1, 1, 1);
    }

    public static Vector3 v255() {
        return new Vector3(255, 255, 255);
    }

    public float x;
    public float y;
    public float z;

    public Vector3() {
    }

    public Vector3(Vector3 v) {
        this.x = v.x;
        this.y = v.y;
        this.z = v.z;
    }

    public Vector3(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Vector3(int color) {
        Color c = new Color(color);

        x = c.getRed();
        y = c.getGreen();
        z = c.getBlue();
    }

    public float magnitude() {
        return (float) Math.abs(Math.sqrt(x * x + y * y + z * z));
    }

    public void subtract(Vector3 toSubtract) {
        x -= toSubtract.x;
        y -= toSubtract.y;
        z -= toSubtract.z;
    }

    public Vector3 add(Vector3 toAdd) {
        x += toAdd.x;
        y += toAdd.y;
        z += toAdd.z;
        return this;
    }

    public void multiply(Vector3 toMultiply) {
        x *= toMultiply.x;
        y *= toMultiply.y;
        z *= toMultiply.z;
    }

    public static Vector3 subtract(Vector3 v, Vector3 toSubtract) {
        return new Vector3(v.x - toSubtract.x, v.y - toSubtract.y, v.z - toSubtract.z);
    }

    public static Vector3 add(Vector3 v, Vector3 toAdd) {
        return new Vector3(v.x + toAdd.x, v.y + toAdd.y, v.z + toAdd.z);
    }

    public static Vector3 multiply(Vector3 v, Vector3 toMultiply) {
        return new Vector3(v.x * toMultiply.x, v.y * toMultiply.y, v.z * toMultiply.z);
    }

    public static Vector3 multiply(Vector3 v, float multiplier) {
        return new Vector3(v.x * multiplier, v.y * multiplier, v.z * multiplier);
    }

    public Color toColor() {
        return new Color(x, y, z);
    }

    public int toIntColor() {
        return new Color((int) x, (int) y, (int) z).getRGB();
    }

    public Vector3(Color color) {
        x = color.getRed();
        y = color.getGreen();
        z = color.getBlue();
    }

    public void clamp(Vector3 min, Vector3 max) {

        x = MathHelper.clamp(x, min.x, max.x);
        y = MathHelper.clamp(y, min.y, max.y);
        z = MathHelper.clamp(z, min.z, max.z);
    }

    public void clamp01() {

        x = MathHelper.clamp(x, 0, 1);
        y = MathHelper.clamp(y, 0, 1);
        z = MathHelper.clamp(z, 0, 1);
    }

    public void clamp0255() {

        x = MathHelper.clamp(x, 0, 255);
        y = MathHelper.clamp(y, 0, 255);
        z = MathHelper.clamp(z, 0, 255);
    }

    public void multiply(float value) {
        x *= value;
        y *= value;
        z *= value;
    }

    public static Color lerp(Color a, Color b, float t) {
        int x = (int) MathHelper.lerp(a.getRed(), b.getRed(), t);
        int y = (int) MathHelper.lerp(a.getGreen(), b.getGreen(), t);
        int z = (int) MathHelper.lerp(a.getBlue(), b.getBlue(), t);

        x = (int) MathHelper.clamp0255(x);
        y = (int) MathHelper.clamp0255(y);
        z = (int) MathHelper.clamp0255(z);

        return new Color(x, y, z);
    }

    public static Vector3 lerp(Vector3 a, Vector3 b, float t) {
        float x = MathHelper.lerp(a.x, b.x, t);
        float y = MathHelper.lerp(a.y, b.y, t);
        float z = MathHelper.lerp(a.z, b.z, t);

        return new Vector3(x, y, z);
    }
}