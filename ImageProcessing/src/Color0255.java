import java.awt.*;

public class Color0255 {

    public static final Color0255 white = new Color0255(255, 255, 255, 255);
    public static final Color0255 black = new Color0255(0, 0, 0, 255);

    public int r;
    public int g;
    public int b;
    public int a;

    public Color0255() {
    }

    public Color0255(int color) {
        Color temp = new Color(color);

        r = MathHelper.clamp0255(temp.getRed());
        g = MathHelper.clamp0255(temp.getGreen());
        b = MathHelper.clamp0255(temp.getBlue());
        a = MathHelper.clamp0255(temp.getAlpha());
    }

    public int toIntColor() {
        return new Color(r, g, b, a).getRGB();
    }

    public Vector3 toVector3() {
        return new Vector3(r, g, b);
    }


    public Color0255(Color01 color) {
        a = (int) color.a * 255;
        r = (int) color.r * 255;
        g = (int) color.g * 255;
        b = (int) color.b * 255;
    }

    public Color0255 multiply(float multiplier) {
        a *= multiplier;
        r *= multiplier;
        g *= multiplier;
        b *= multiplier;
        return this;
    }

    public Color0255(Vector3 color) {
        r = MathHelper.clamp0255((int) color.x);
        g = MathHelper.clamp0255((int) color.y);
        b = MathHelper.clamp0255((int) color.z);
        a = 255;
    }

    public Color0255 add(Color0255 color) {
        r += color.r;
        g += color.g;
        b += color.b;
        a += color.a;
        return this;
    }

    public Color0255 add(Vector3 v) {
        r += v.x;
        g += v.y;
        b += v.z;
        return this;
    }

    public Color0255 clamp() {
        r = MathHelper.clamp0255(r);
        g = MathHelper.clamp0255(g);
        b = MathHelper.clamp0255(b);
        a = MathHelper.clamp0255(a);
        return this;
    }


    public Color0255(int r, int g, int b) {
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = 255;
    }

    public Color0255(int r, int g, int b, int a) {
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a;
    }
}
