/**
 * Created by Gustavo on 27/03/2017.
 */
public class Kernel {

    public static float[][] createBlurKernel(int size) {
        float[][] blur = new float[size][size];
        float squareSize = size * size;
        float intensity = 1f / squareSize;

        for (int y = 0; y < size; y++)
            for (int x = 0; x < size; x++)
                blur[x][y] = intensity;

        return blur;
    }

    public static final float[][] laplace = {
            {0.0f, -1.0f, 0.0f},
            {-1.0f, 4.0f, -1.0f},
            {0.0f, -1.0f, 0.0f}
    };
}
