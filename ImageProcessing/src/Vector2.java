import java.awt.*;

public class Vector2 {
    public static Vector2 zero() {
        return new Vector2(0, 0);
    }

    public static Vector2 one() {
        return new Vector2(1, 1);
    }

    public float x;
    public float y;

    public Vector2() {
    }

    public Vector2(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public Vector2(int color) {
        Color c = new Color(color);

        x = c.getRed();
        y = c.getGreen();
    }

    public float magnitude() {
        return (float) Math.abs(Math.sqrt(x * x + y * y));
    }

    public void subtract(Vector2 toSubtract) {
        x -= toSubtract.x;
        y -= toSubtract.y;
    }

    public void add(Vector2 toAdd) {
        x += toAdd.x;
        y += toAdd.y;
    }

    public Vector2 normalized() {
        x /= magnitude();
        y /= magnitude();

        return this;
    }

    public void multiply(Vector2 toMultiply) {
        x *= toMultiply.x;
        y *= toMultiply.y;
    }

    public static Vector2 subtract(Vector2 v, Vector2 toSubtract) {
        return new Vector2(v.x - toSubtract.x, v.y - toSubtract.y);
    }

    public static Vector2 add(Vector2 v, Vector2 toAdd) {
        return new Vector2(v.x + toAdd.x, v.y + toAdd.y);
    }

    public static Vector2 multiply(Vector2 v, Vector2 toMultiply) {
        return new Vector2(v.x * toMultiply.x, v.y * toMultiply.y);
    }

    public void clamp(Vector2 min, Vector2 max) {

        x = MathHelper.clamp(x, min.x, max.x);
        y = MathHelper.clamp(y, min.y, max.y);
    }

    public void clamp01() {

        x = MathHelper.clamp(x, 0, 1);
        y = MathHelper.clamp(y, 0, 1);
    }

    public void clamp0255() {

        x = MathHelper.clamp(x, 0, 255);
        y = MathHelper.clamp(y, 0, 255);
    }

    public void multiply(float value) {
        x *= value;
        y *= value;
    }

    public static Vector2 lerp(Vector2 a, Vector2 b, float t) {
        float x = MathHelper.lerp(a.x, b.x, t);
        float y = MathHelper.lerp(a.y, b.y, t);

        return new Vector2(x, y);
    }
}