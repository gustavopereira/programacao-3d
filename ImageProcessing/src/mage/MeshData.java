package mage;

import java.util.ArrayList;

import org.joml.Vector2f;
import org.joml.Vector3f;

public class MeshData {
    public ArrayList<Vector3f> vertices = new ArrayList<>();
    public ArrayList<Integer> triangles = new ArrayList<>();
    public ArrayList<Vector2f> uv = new ArrayList<>();
    public ArrayList<Vector3f> colors = new ArrayList<>();

    public MeshData() {
    }

    public MeshData(ArrayList<Vector3f> vertices, ArrayList<Integer> triangles, ArrayList<Vector2f> uv, ArrayList<Vector3f> colors) {
        this.vertices = vertices;
        this.triangles = triangles;
        this.uv = uv;
        this.colors = colors;
    }

    public void setData(MeshData data) {
        vertices = data.vertices;
        triangles = data.triangles;
        uv = data.uv;
        colors = data.colors;
    }

    public void setData(ArrayList<Vector3f> vertices, ArrayList<Integer> triangles, ArrayList<Vector2f> uv, ArrayList<Vector3f> colors) {
        this.vertices = vertices;
        this.triangles = triangles;
        this.uv = uv;
        this.colors = colors;
    }

    public void append(ArrayList<Vector3f> vertices, ArrayList<Integer> triangles, ArrayList<Vector2f> uv, ArrayList<Vector3f> colors) {
        if (vertices.size() <= 0)//Received mesh in null
            return;

        else if (this.vertices.size() < 0) // When this.mesh is nothing
        {
            setData(vertices, triangles, uv, colors);
            return;
        } else {
            int vertexCount = this.vertices.size();
            for (Integer triangle : triangles)
                this.triangles.add(triangle + vertexCount);

            this.vertices.addAll(vertices);
            if(uv != null) this.uv.addAll(uv);
            this.colors.addAll(colors);
        }
    }

    public void append(MeshData data) {
        if (data.vertices.size() <= 0)//Received mesh in null
            return;

        else if (vertices.size() < 0) // Current mesh is nothing
        {
            setData(data);
            return;
        } else {
            int vertexCount = vertices.size();
            for (Integer triangle : data.triangles)
                triangles.add(triangle + vertexCount);

            vertices.addAll(data.vertices);
            if(uv != null) uv.addAll(data.uv);
            colors.addAll(data.colors);
        }
    }

    public void addOffset(Vector3f offset) {
        for (int i = 0; i < vertices.size(); i++)
            vertices.get(i).add(offset);
    }
}

