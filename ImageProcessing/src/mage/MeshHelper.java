package mage;

import org.joml.Vector2f;
import org.joml.Vector3f;

import java.util.ArrayList;
import java.util.Arrays;

public class MeshHelper {

    private static final float black = 0.6f;
    private static final int ATLAS_SIZE = 16;

    private static final Vector3f shadowColor = new Vector3f(black, black, black);

    public static MeshData createQuad(float size, float x, float y, float z, Vector3f color) {
        MeshData meshData = new MeshData();

        ArrayList<Vector3f> vertices;
        ArrayList<Integer> triangles;
        ArrayList<Vector2f> uv;
        ArrayList<Vector3f> colors;

        float halfSize = size / 2f;

        vertices = new ArrayList<>(Arrays.asList(new Vector3f[]{
                new Vector3f(-halfSize, 0, -halfSize),
                new Vector3f(halfSize, 0, -halfSize),
                new Vector3f(halfSize, 0, halfSize),
                new Vector3f(-halfSize, 0, halfSize)
        }));

        triangles = new ArrayList<>(Arrays.asList(new Integer[]{
                2, 1, 0,
                3, 2, 0
        }));

        uv = new ArrayList<>(Arrays.asList(new Vector2f[]{
                new Vector2f(0, 0),
                new Vector2f(1, 0),
                new Vector2f(1, 1),
                new Vector2f(0, 1)
        }));

        colors = new ArrayList<>(Arrays.asList(new Vector3f[]{
                color,
                color,
                color,
                color
        }));

        meshData.append(vertices, triangles, uv, colors);
        meshData.addOffset(new Vector3f(x, y, z));
        return meshData;
    }

    public static MeshData createQuadHeight(float[][] offset, int x, int z) {
        MeshData meshData = new MeshData();

        ArrayList<Vector3f> vertices;
        ArrayList<Integer> triangles;
        ArrayList<Vector2f> uv;
        ArrayList<Vector3f> colors;

        float halfSize = 0.5f;

        vertices = new ArrayList<>(Arrays.asList(new Vector3f[]{
                new Vector3f(-halfSize, offset[x][z], -halfSize),
                new Vector3f(halfSize, offset[x+1][z], -halfSize),
                new Vector3f(halfSize, offset[x+1][z+1], halfSize),
                new Vector3f(-halfSize, offset[x][z+1], halfSize)
        }));

        triangles = new ArrayList<>(Arrays.asList(new Integer[]{
                2, 1, 0,
                3, 2, 0
        }));

        uv = new ArrayList<>(Arrays.asList(new Vector2f[]{
                new Vector2f(0, 0),
                new Vector2f(1, 0),
                new Vector2f(1, 1),
                new Vector2f(0, 1)
        }));

        float value = offset[x][z];

        Vector3f color = new Vector3f(value, value, value);

        colors = new ArrayList<>(Arrays.asList(new Vector3f[]{
                color,
                color,
                color,
                color
        }));

        meshData.append(vertices, triangles, uv, colors);
        meshData.addOffset(new Vector3f(x, 0, z));
        return meshData;
    }

    public static MeshData createPentagon(Vector3f[] color) {
        MeshData meshData = new MeshData();

        ArrayList<Vector3f> vertices;
        ArrayList<Integer> triangles;
        ArrayList<Vector2f> uv;
        ArrayList<Vector3f> colors;

        vertices = new ArrayList<>(Arrays.asList(new Vector3f[]{
                new Vector3f(-.5f, 0, 0),   //0
                new Vector3f(.5f, 0, 0),    //1
                new Vector3f(.7f, .65f, 0),  //2
                new Vector3f(0, 1, 0),      //3
                new Vector3f(-.7f, .65f, 0), //4
                new Vector3f(0f, .4f, 0),   //5 center
        }));

        triangles = new ArrayList<>(Arrays.asList(new Integer[]{
                0, 1, 5,
                1, 2, 5,
                2, 3, 5,
                3, 4, 5,
                4, 0, 5
        }));

        uv = new ArrayList<>(Arrays.asList(new Vector2f[]{
        }));

        colors = new ArrayList<>(Arrays.asList(new Vector3f[]{
                color[0],
                color[1],
                color[2],
                color[3],
                color[4],
                color[5]
        }));

        meshData.append(vertices, triangles, uv, colors);
        meshData.addOffset(new Vector3f(0, -.5f, 0));
        return meshData;
    }
}

