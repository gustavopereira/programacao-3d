import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

public class Program {

    public static void main(String[] args) throws IOException {
        //Loading Files
        File errorsB1 = Paths.get(".", "resources", "errorsB1.png").normalize().toFile();
        File errorsB2 = Paths.get(".", "resources", "errorsB2.png").normalize().toFile();
        File mountain = Paths.get(".", "resources", "mountain.jpg").normalize().toFile();
        File part1 = Paths.get(".", "resources", "part1.png").normalize().toFile();
        File part2 = Paths.get(".", "resources", "part2.png").normalize().toFile();
        File puppy = Paths.get(".", "resources", "puppy.png").normalize().toFile();
        File sunset = Paths.get(".", "resources", "sunset.png").normalize().toFile();

        //Brighter
        Texture2D brighter = new Texture2D(puppy);
        brighter.saturate(3);
        brighter.saveInDisc("png", "Brighter");

        //Darker
        Texture2D darker = new Texture2D(puppy);
        darker.saturate(.4f);
        darker.saveInDisc("png", "Darker");

        //Simple Grayscale
        Texture2D simplyGrayscale = new Texture2D(puppy);
        simplyGrayscale.toGrayscale(Texture2D.GrayscaleType.Simple);
        simplyGrayscale.saveInDisc("png", "SimpleGrayscale");

        //Average Grayscale
        Texture2D avgGrayscale = new Texture2D(puppy);
        avgGrayscale.toGrayscale(Texture2D.GrayscaleType.Average);
        avgGrayscale.saveInDisc("png", "AverageGrayscale");

        //Complex Grayscale
        Texture2D complexGrayscale = new Texture2D(puppy);
        complexGrayscale.toGrayscale(Texture2D.GrayscaleType.Complex);
        complexGrayscale.saveInDisc("png", "ComplexGrayscale");

        //Threshold
        Texture2D Thresholded = new Texture2D(puppy);
        Thresholded.threshold(new Color01(.5f, .5f, .5f).toIntColor());
        Thresholded.saveInDisc("png", "Thresholded");

        //b1 - b2
        Texture2D b1Subtracted = new Texture2D(errorsB1);
        b1Subtracted.subtract(new Texture2D(errorsB2));
        b1Subtracted.saveInDisc("png", "B1 Subtracted");

        //b2 - b1
        Texture2D b2Subtracted = new Texture2D(errorsB2);
        b2Subtracted.subtract(new Texture2D(errorsB1));
        b2Subtracted.saveInDisc("png", "B2 Subtracted");

        //Add
        Texture2D added = new Texture2D(errorsB1);
        added.add(new Texture2D(errorsB2));
        added.saveInDisc("png", "Added");

        //Lerp
        Texture2D lerped = new Texture2D(part1);
        lerped.lerp(new Texture2D(part2), .5f);
        lerped.saveInDisc("png", "Lerped");

        //Multiply
        Texture2D multiplied = new Texture2D(puppy);
        multiplied.multiply(new Color01(1.4f, 1, 1));
        multiplied.saveInDisc("png", "Multiplied");

        //Palette EGA16
        Texture2D puppyEGA16 = new Texture2D(puppy);
        puppyEGA16.convertToPallet(new EGA16());
        puppyEGA16.saveInDisc("png", "PuppyEGA16");

        //Palette EGA64
        Texture2D puppyEGA64 = new Texture2D(puppy);
        puppyEGA64.convertToPallet(new EGA64());
        puppyEGA64.saveInDisc("png", "PuppyEGA64");

        //Floyd Dithering EGA16
        Texture2D puppyEGA16Dithered = new Texture2D(puppy);
        puppyEGA16Dithered.floydDithering(new EGA16());
        puppyEGA16Dithered.saveInDisc("png", "PuppyEGA16Dithered");

        //Floyd Dithering EGA64
        Texture2D puppyEGA64Dithered = new Texture2D(puppy);
        puppyEGA64Dithered.floydDithering(new EGA64());
        puppyEGA64Dithered.saveInDisc("png", "PuppyEGA64Dithered");

        //Draw Line #1
        Texture2D line1 = Texture2D.Create(100, 100, Color0255.white);
        line1.drawLine(new Vector2(1, 1), new Vector2(1, 100), Color0255.black);
        line1.saveInDisc("png", "Line1");

        //Draw Line #2
        Texture2D line2 = Texture2D.Create(100, 100, Color0255.white);
        line2.drawLine(new Vector2(100, 1), new Vector2(1, 1), Color0255.black);
        line2.saveInDisc("png", "Line2");

        //Kernel Laplace
        Texture2D kernelLaplace = new Texture2D(puppy);
        kernelLaplace.applyKernel(Kernel.laplace);
        kernelLaplace.saveInDisc("png", "KernelLaplace");

        //Pixelate
        Texture2D pixelate = new Texture2D(puppy);
        pixelate.pixelate(10);
        pixelate.saveInDisc("png", "Pixelated");

        //Histogram Equalization
        Texture2D histogramEqualization = new Texture2D(mountain);
        histogramEqualization.equalize();
        histogramEqualization.saveInDisc("png", "MontanhaEqualized");

        //Bloom
        Texture2D bloom = new Texture2D(sunset);
        bloom.bloom(204, 0.1f); //threshold = 0.8[0...1] * 255[0...255]
        bloom.saveInDisc("png", "SunsetBloom");
    }
}
