public class EGA16 extends ColorPalette {
    public EGA16() {
        palette = new int[]{ // 4bit pallete
                0x000000, 0x800000, 0xFF0000, 0xFF00FF,
                0xFF8080, 0x008000, 0x00FF00, 0x00FFFF,
                0x000080, 0x800080, 0x0000FF, 0xC0C0C0,
                0x808080, 0x808000, 0xFFFF00, 0xFFFFFF
        };
    }
}
