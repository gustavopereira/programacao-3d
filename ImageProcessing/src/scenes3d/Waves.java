package scenes3d;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;

import cg.FPSCamera;
import cg.MeshFactory;
import mage.Keyboard;
import mage.Mesh;
import mage.Scene;
import mage.Window;
import org.joml.Matrix4f;
import org.joml.Vector3f;

public class Waves implements Scene {
    private Keyboard keys = Keyboard.getInstance();

    private Mesh mesh;
    private FPSCamera camera = new FPSCamera();
    private float elapsedTime;

    private static Window window;
    private static final String titleFormat = "Waves Shader |  FPS:  %d";

    @Override
    public void init() {
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        glEnable(GL_CULL_FACE);
        glEnable(GL_DEPTH_TEST);
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

        mesh = MeshFactory.createQuads(.4f, 128, 128, new Vector3f(1, 1, 1), "wave");
        camera.setPosition(new Vector3f(0, 1.3f, 2));
    }

    private void HandleCameraInput(float secs) {
        if (keys.isDown(GLFW_KEY_A)) camera.strafe(1, secs);
        if (keys.isDown(GLFW_KEY_D)) camera.strafe(-1, secs);
        if (keys.isDown(GLFW_KEY_W)) camera.move(1, secs);
        if (keys.isDown(GLFW_KEY_S)) camera.move(-1, secs);
        if (keys.isDown(GLFW_KEY_E)) camera.height(1, secs);
        if (keys.isDown(GLFW_KEY_Q)) camera.height(-1, secs);
        if (keys.isDown(GLFW_KEY_UP)) camera.rotateX(-1 * secs);
        if (keys.isDown(GLFW_KEY_DOWN)) camera.rotateX(1 * secs);
        if (keys.isDown(GLFW_KEY_RIGHT)) camera.rotate(-1 * secs);
        if (keys.isDown(GLFW_KEY_LEFT)) camera.rotate(1 * secs);
    }

    @Override
    public void update(float secs) {
        elapsedTime += secs;

        if (keys.isPressed(GLFW_KEY_ESCAPE)) {
            glfwSetWindowShouldClose(glfwGetCurrentContext(), GL_TRUE);
            return;
        }

        HandleCameraInput(secs);

        mesh.setUniform("uElapsedTime", elapsedTime);

        if (window != null)
            glfwSetWindowTitle(window.getWindow(), String.format(titleFormat, (int) (1f / secs)));
    }

    @Override
    public void draw() {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        mesh.getShader().bind()
                .setUniform("uProjection", camera.getProjectionMatrix())
                .setUniform("uView", camera.getViewMatrix())
                .unbind();
        mesh.setUniform("uWorld", new Matrix4f());
        mesh.draw();
    }

    @Override
    public void deinit() {
    }

    public static void main(String[] args) {
      window=  new Window(new Waves(), "Waves Shader", 1920, 1080);
      window.show();
    }
}
