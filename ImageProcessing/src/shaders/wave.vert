#version 330

uniform mat4 uProjection;
uniform mat4 uView;
uniform mat4 uWorld;
uniform float uElapsedTime;

in vec3 aPosition;
in vec3 aColor;

out vec4 vColor;

void main(){
    vec3 aPos = aPosition;
    aPos.y = (sin((aPos.x + uElapsedTime * .1 - 1) * .1) + cos((aPos.z + uElapsedTime*.1 + 4 ) * .2)) * .2f;
    aPos.y += (sin((aPos.x + uElapsedTime *5+ 5) *1 ) + cos((aPos.z + uElapsedTime *5+ 2 ) * .03f)) * .1f;
    aPos.y += (sin((aPos.x + uElapsedTime *1- 4) *.2) + cos((aPos.z + uElapsedTime *1+7) * 2)) * .2f;
    aPos.y += (sin((aPos.x + uElapsedTime *.5- 4) *.04) + cos((aPos.z + uElapsedTime *.5 +7) * 0.04)) * .4f;

    vec4 worldPos = uWorld * vec4(aPos, 1.0f);
    gl_Position =  uProjection * uView * worldPos;

    vec4 red =   vec4(1, 0, 0, 0);
    vec4 green = vec4(0, 1, 0, 0);
    vec4 blue =  vec4(0, 0, 1, 0);

     float t = 1 - aPos.y * 2;
     vec4 lerpedColor = vec4(0,0,0,0);
     if(aPos.y > 0) lerpedColor = red + (green - red) *t;
     else           lerpedColor = blue + (blue - blue) *t;

    vColor = vec4(aColor,1) * lerpedColor;
}