#version 330

uniform mat4 uProjection;
uniform mat4 uView;
uniform mat4 uWorld;

uniform vec3 uCameraPosition;

// Point light
uniform vec3 uLightPosition;

in vec3 aPosition;
in vec3 aNormal;
in vec3 aColor;

out vec4 vColor;
out vec3 vNormal;
out vec3 vViewPath;

out float vertDist;
out vec3 vLightDir;

void main() {

    vColor = vec4(aColor, 1.0f);

    vNormal = (uWorld * vec4(aNormal, 0.0)).xyz;

    vec4 worldPos = uWorld * vec4(aPosition, 1.0f);
    gl_Position =  uProjection * uView * worldPos;

    vNormal = (uWorld * vec4(aNormal, 0.0)).xyz;
    vViewPath = uCameraPosition - worldPos.xyz;

    vLightDir = worldPos.xyz - uLightPosition;
    vertDist = distance(uLightPosition, worldPos.xyz);
}