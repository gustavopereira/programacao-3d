#version 330

uniform mat4 uProjection;
uniform mat4 uView;
uniform mat4 uWorld;

uniform float uAmplitude;

in vec3 aPosition;
//in vec3 aColor;

out vec4 vColor;

void main() {
    // Pega a posição para trabalhar nela
    vec3 aPos = aPosition;


    vec4 blue = vec4(0,0,0,1);
    vec4 red  = vec4(1,1,1,1);

    float t = aPos.y/uAmplitude;
    vec4 lerpedColor = vec4(0,0,0,0);
    lerpedColor = blue + (red - blue) *t;


    vec4 worldPos = uWorld * vec4(aPos, 1.0f);
    gl_Position =  uProjection * uView * worldPos;

    vColor = vec4(lerpedColor);
}