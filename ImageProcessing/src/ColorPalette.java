/**
 * Created by Gustavo on 10/03/2017.
 */
public class ColorPalette {
    public static int[] palette = {};

    public Color0255 getNearestInPallet(Color0255 target) {

        Vector3 targetVector = target.toVector3();
        Vector3 nearestVector = Vector3.zero();
        float nearestDistance = Vector3.v255().magnitude();

        float d;
        for (int i = 0; i < palette.length; i++) {
            Vector3 palletColor = new Color0255(palette[i]).toVector3();

            Vector3 diffVector = Vector3.subtract(palletColor, targetVector);
            d = diffVector.magnitude();

            if (d < nearestDistance) {
                nearestDistance = d;
                nearestVector = palletColor;
            }

        }
        return new Color0255(nearestVector);
    }
}
