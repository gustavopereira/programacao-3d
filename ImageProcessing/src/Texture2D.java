import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Random;

public class Texture2D {

    // Variables
    private BufferedImage bufferedImage;

    public enum GrayscaleType {Simple, Average, Complex}


    // Constructors
    public Texture2D(BufferedImage bufferedImage) {
        this.bufferedImage = bufferedImage;
    }

    public Texture2D(String path, String fileName) throws IOException {
        bufferedImage = ImageIO.read(new File(path, fileName));
    }

    public Texture2D(File image) throws IOException {
        bufferedImage = ImageIO.read(image);
    }

    public Texture2D(int width, int height) {
        bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
    }


    // Getters
    public BufferedImage getBufferedImage() {
        return bufferedImage;
    }

    public int getWidth() {
        return bufferedImage.getWidth();
    }

    public int getHeight() {
        return bufferedImage.getHeight();
    }

    public Color0255 getPixel(int x, int y) {
        return new Color0255(bufferedImage.getRGB(x, y));
    }

    public int getType() {
        return bufferedImage.getType();
    }


    // Setters
    public void setPixel(int x, int y, Color01 color) {
        if (x >= 0 && y >= 0 && x < getWidth() && y < getHeight())
            bufferedImage.setRGB(x, y, color.toIntColor());
        else
            System.out.println("Pixel out of bounds: " + x + " | " + y);
    }

    public void setPixel(int x, int y, Color0255 color) {
        if (x >= 0 && y >= 0 && x < getWidth() && y < getHeight())
            bufferedImage.setRGB(x, y, color.toIntColor());
        else
            System.out.println("Pixel out of bounds: " + x + " | " + y);
    }


    // Post Processing
    public void convertToPallet(ColorPalette palette) {
        Vector3 pixel;
        Color0255 pixelColor;
        Color0255 color;
        for (int y = 0; y < bufferedImage.getHeight(); y++)
            for (int x = 0; x < bufferedImage.getWidth(); x++) {
                pixel = new Vector3(bufferedImage.getRGB(x, y));

                pixelColor = new Color0255(pixel);

                color = palette.getNearestInPallet(pixelColor);

                setPixel(x, y, color);
            }
    }

    public void drawLine(Vector2 p1, Vector2 p2, Color0255 color) {
        Vector2 dir = Vector2.subtract(p1, p2);
        int distance = (int) dir.magnitude();

        int iteractions = distance * 2; // * 2 to maximize samples and minimize blank spaces in line
        float itr = 1f / iteractions;
        float t = 0;

        for (int i = 0; i < iteractions; i++) {
            Vector2 p = Vector2.lerp(p1, p2, t);
            setPixel((int) p.x, (int) p.y, color);
            t += itr;
        }

    }

    public void floydDithering(ColorPalette palette) {
        int w = getWidth();
        int h = getHeight();

        Vector3[][] pixels = new Vector3[w][h];

        for (int y = 0; y < h; y++)
            for (int x = 0; x < w; x++)
                pixels[x][y] = getPixel(x, y).toVector3();


        for (int y = 0; y < h; y++) {
            for (int x = 0; x < w; x++) {

                Vector3 oldColor = pixels[x][y];
                Vector3 newColor = palette.getNearestInPallet(new Color0255(oldColor)).toVector3();
                setPixel(x, y, new Color0255(newColor));

                Vector3 err = Vector3.subtract(oldColor, newColor);

                if (x + 1 < w) {
                    err.multiply(7f / 16);
                    Vector3 c = pixels[x + 1][y];
                    c.add(err);
                    c.clamp0255();
                    pixels[x + 1][y] = c;
                }

                if (x - 1 >= 0 && y + 1 < h) {
                    err.multiply(3f / 16);
                    Vector3 c = pixels[x - 1][y + 1];
                    c.add(err);
                    c.clamp0255();
                    pixels[x - 1][y + 1] = c;
                }

                if (y + 1 < h) {
                    err.multiply(5f / 16);
                    Vector3 c = pixels[x][y + 1];
                    c.add(err);
                    c.clamp0255();
                    pixels[x][y + 1] = c;
                }

                if (x + 1 < w && y + 1 < h) {
                    err.multiply(1f / 16);
                    Vector3 c = pixels[x + 1][y + 1];
                    c.add(err);
                    c.clamp0255();
                    pixels[x + 1][y + 1] = c;
                }
            }
        }
    }

    public void saturate(float intensity) {
        intensity = MathHelper.clamp(intensity < 0 ? 1 + intensity : intensity, 0, 255);

        Vector3 color;
        for (int y = 0; y < bufferedImage.getHeight(); y++)
            for (int x = 0; x < bufferedImage.getWidth(); x++) {
                color = new Vector3(bufferedImage.getRGB(x, y));

                color.multiply(intensity);
                color.clamp0255();

                setPixel(x, y, new Color0255(color));
            }
    }

    public void toGrayscale(GrayscaleType type) {
        switch (type) {
            case Simple:
                simpleGrayscale();
                break;
            case Average:
                averageGrayscale();
                break;
            case Complex:
                complexGrayscale();
                break;
        }
    }

    public void subtract(Texture2D toSubtract) {
        Vector3 myColor;
        Vector3 toSubColor;
        Vector3 resultColor;
        for (int y = 0; y < bufferedImage.getHeight(); y++)
            for (int x = 0; x < bufferedImage.getWidth(); x++) {

                myColor = new Vector3(bufferedImage.getRGB(x, y));
                toSubColor = new Vector3(toSubtract.bufferedImage.getRGB(x, y));

                resultColor = Vector3.subtract(myColor, toSubColor);
                resultColor.clamp0255();

                setPixel(x, y, new Color0255(resultColor));
            }
    }

    public void add(Texture2D toAdd) {
        Vector3 myColor;
        Vector3 toAddColor;
        Vector3 resultColor;
        for (int y = 0; y < bufferedImage.getHeight(); y++)
            for (int x = 0; x < bufferedImage.getWidth(); x++) {

                myColor = new Vector3(bufferedImage.getRGB(x, y));
                toAddColor = new Vector3(toAdd.bufferedImage.getRGB(x, y));

                resultColor = Vector3.add(myColor, toAddColor);
                resultColor.clamp0255();

                setPixel(x, y, new Color0255(resultColor));
            }
    }

    public void multiply(Color01 multiplier) {
        Vector3 color0255;
        Color01 color01;
        Color01 resultColor;
        for (int y = 0; y < bufferedImage.getHeight(); y++)
            for (int x = 0; x < bufferedImage.getWidth(); x++) {
                color0255 = new Vector3(bufferedImage.getRGB(x, y));
                color01 = new Color01(new Color0255(color0255));

                Vector3 resultVector = Vector3.multiply(multiplier.toVector3(), color01.toVector3());
                resultVector.clamp01();

                resultColor = new Color01(resultVector);
                setPixel(x, y, resultColor);
            }
    }

    public void lerp(Texture2D to, float t) {
        Vector3 myColor;
        Vector3 toColor;
        Vector3 resultColor;
        for (int y = 0; y < bufferedImage.getHeight(); y++)
            for (int x = 0; x < bufferedImage.getWidth(); x++) {
                myColor = new Vector3(bufferedImage.getRGB(x, y));
                toColor = new Vector3(to.bufferedImage.getRGB(x, y));
                resultColor = Vector3.lerp(myColor, toColor, t);

                setPixel(x, y, new Color0255(resultColor));
            }
    }

    public void threshold(int value) {

        Color0255 valueColor = new Color0255(value);
        float valueMagnitude = valueColor.toVector3().magnitude();

        for (int y = 0; y < bufferedImage.getHeight(); y++)
            for (int x = 0; x < bufferedImage.getWidth(); x++) {
                Color0255 pixel = new Color0255(bufferedImage.getRGB(x, y));
                float pixelMagnitude = pixel.toVector3().magnitude();

                if (pixelMagnitude >= valueMagnitude)
                    setPixel(x, y, Color0255.white);
                else
                    setPixel(x, y, Color0255.black);
            }
    }

    public void applyKernel(float[][] kernel) {
        BufferedImage out = new BufferedImage(getWidth(), getHeight(), getType());
        for (int y = 0; y < getHeight(); y++) {
            for (int x = 0; x < getWidth(); x++) {

                Color0255 newColor = new Color0255(0, 0, 0, 255);

                for (int kx = 0; kx < kernel.length; kx++) {
                    for (int ky = 0; ky < kernel[kx].length; ky++) {

                        int px = x + (kx - 1);
                        int py = y + (ky - 1);

                        if (px < 0 || px >= getWidth() || py < 0 || py >= getHeight())
                            continue;

                        Color0255 color = getPixel(px, py);

                        float mul = kernel[kx][ky];
                        newColor.add(color.multiply(mul));
                    }
                }

                out.setRGB(x, y, newColor.clamp().toIntColor());
            }
        }
        bufferedImage = out;
    }

    public void pixelate(int pixelSize) {

        int xTimes = (int) Math.ceil((float) getWidth() / pixelSize);
        int yTimes = (int) Math.ceil((float) getHeight() / pixelSize);

        for (int y = 0; y < yTimes; y++)
            for (int x = 0; x < xTimes; x++) {
                Color0255 macroColor = getMacroPixel(x * pixelSize, y * pixelSize, pixelSize);
                setMacroPixel(x * pixelSize, y * pixelSize, pixelSize, macroColor);
            }

    }

    public void equalize() {
        int[] colorMap = getColorMap(getHistogram());

        for (int y = 0; y < getHeight(); y++)
            for (int x = 0; x < getWidth(); x++) {
                Color0255 color = getPixel(x, y);
                int gray = colorMap[color.r];
                setPixel(x, y, new Color0255(gray, gray, gray));
            }
    }

    public void bloom(int threshold, float intensity) {
        Texture2D extracted = Texture2D.Create(getWidth(), getHeight(), new Color0255(0, 0, 0, 0));
        Texture2D blur5 = Texture2D.Create(getWidth(), getHeight(), new Color0255(0, 0, 0, 0));
        Texture2D blur11 = Texture2D.Create(getWidth(), getHeight(), new Color0255(0, 0, 0, 0));
        Texture2D blur21 = Texture2D.Create(getWidth(), getHeight(), new Color0255(0, 0, 0, 0));
        Texture2D blur41 = Texture2D.Create(getWidth(), getHeight(), new Color0255(0, 0, 0, 0));

        //Extracting pixels
        for (int y = 0; y < getHeight(); y++)
            for (int x = 0; x < getWidth(); x++) {
                Color0255 pixel = getPixel(x, y);
                if (pixel.toVector3().magnitude() > threshold)
                    extracted.setPixel(x, y, pixel);
                else
                    extracted.setPixel(x, y, Color0255.black);
            }


        blur5.bufferedImage = blur11.bufferedImage = blur21.bufferedImage = blur41.bufferedImage = extracted.bufferedImage;

        blur5.applyKernel(Kernel.createBlurKernel(5));
        blur11.applyKernel(Kernel.createBlurKernel(11));
        blur21.applyKernel(Kernel.createBlurKernel(21));
        blur41.applyKernel(Kernel.createBlurKernel(41));

        blur5.saturate(intensity);
        blur11.saturate(intensity);
        blur21.saturate(intensity);
        blur41.saturate(intensity);

        add(blur5);
        add(blur11);
        add(blur21);
        add(blur41);
    }

    // Private Methods
    private void simpleGrayscale() {
        Vector3 color;
        for (int y = 0; y < bufferedImage.getHeight(); y++)
            for (int x = 0; x < bufferedImage.getWidth(); x++) {
                color = new Vector3(bufferedImage.getRGB(x, y));

                color.y = color.z = color.x;

                setPixel(x, y, new Color0255(color));
            }
    }

    private void averageGrayscale() {
        Vector3 color;
        for (int y = 0; y < bufferedImage.getHeight(); y++)
            for (int x = 0; x < bufferedImage.getWidth(); x++) {
                color = new Vector3(bufferedImage.getRGB(x, y));

                float avg = (color.x + color.y + color.z) / 3f;
                color.x = color.y = color.z = avg;

                setPixel(x, y, new Color0255(color));
            }
    }

    private Color0255 getMacroPixel(int x, int y, int size) {
        Color0255 avg = new Color0255();
        int endX = (x + size) < getWidth() ? x + size : getWidth() - 1;
        int endY = (y + size) < getHeight() ? y + size : getHeight() - 1;

        for (int py = y; py < endY; py++)
            for (int px = x; px < endX; px++)
                avg.add(getPixel(x, y));

        float samples = size * size;
        avg.r /= samples;
        avg.g /= samples;
        avg.b /= samples;

        avg.clamp();
        return avg;
    }

    private void setMacroPixel(int x, int y, int size, Color0255 color) {
        int endX = (x + size) < getWidth() ? x + size : getWidth() - 1;
        int endY = (y + size) < getHeight() ? y + size : getHeight() - 1;

        for (int py = y; py < endY; py++)
            for (int px = x; px < endX; px++)
                setPixel(px, py, color);
    }

    private void complexGrayscale() {
        Vector3 color;
        for (int y = 0; y < bufferedImage.getHeight(); y++)
            for (int x = 0; x < bufferedImage.getWidth(); x++) {
                color = new Vector3(bufferedImage.getRGB(x, y));

                int value = (int) (.3f * color.x + .59f * color.y + .11f * color.z);
                color = new Vector3(value, value, value);

                setPixel(x, y, new Color0255(color));
            }
    }

    private int[] getHistogram() {
        int[] histogram = new int[256];
        for (int y = 0; y < getHeight(); y++)
            for (int x = 0; x < getWidth(); x++) {
                Color0255 color = getPixel(x, y);
                histogram[color.r] += 1;
            }

        return histogram;
    }

    private int[] getAccumulatedHistogram(int[] histogram) {
        int[] accumulated = new int[256];
        accumulated[0] = histogram[0];
        for (int i = 1; i < histogram.length; i++)
            accumulated[i] = histogram[i] + accumulated[i - 1];
        return accumulated;
    }

    private int getFirstIncidence(int[] histogram) {
        for (int i = 0; i < histogram.length; i++)
            if (histogram[i] != 0)
                return histogram[i];
        return 0;
    }

    private int[] getColorMap(int[] histogram) {
        int pixelCount = getWidth() * getHeight();
        int[] colorMap = new int[256];
        int[] accumulated = getAccumulatedHistogram(histogram);
        float firstIncidence = getFirstIncidence(histogram);
        for (int i = 0; i < histogram.length; i++)
            colorMap[i] = Math.round(((accumulated[i] - firstIncidence) / (pixelCount - firstIncidence)) * 255);
        return colorMap;
    }

    private BufferedImage Copy(BufferedImage bufferedImage) {
        int w = bufferedImage.getWidth();
        int h = bufferedImage.getHeight();

        BufferedImage copy = new BufferedImage(w, h, bufferedImage.getType());

        for (int y = 0; y < h; y++)
            for (int x = 0; x < w; x++)
                copy.setRGB(x, y, bufferedImage.getRGB(x, y));

        return copy;
    }


    // Static Methods
    public static Texture2D Create(int width, int height, Color0255 bgColor) {
        BufferedImage out = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        FillImage(out, bgColor);
        return new Texture2D(out);
    }

    public static void Gradient(BufferedImage img, Color a, Color b) {
        float lenght = img.getHeight() * img.getWidth();
        int interactions = 0;
        float t = 0;

        for (int y = 0; y < img.getHeight(); y++)
            for (int x = 0; x < img.getWidth(); x++) {
                Color color = Vector3.lerp(a, b, t);
                img.setRGB(x, y, color.getRGB());

                interactions++;
                t = (float) interactions / lenght;
                t = MathHelper.clamp(t, 0, 1);
            }
    }

    public static void FillImage(BufferedImage img, Color0255 color) {
        for (int y = 0; y < img.getHeight(); y++)
            for (int x = 0; x < img.getWidth(); x++)
                img.setRGB(x, y, color.toIntColor());
    }

    public static BufferedImage Create(int width, int height) {
        BufferedImage out = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        return out;
    }

    // Other Methods
    public void saveInDisc(String format, String fileName) throws IOException {
        ImageIO.write(bufferedImage, format, new File("processed/"+ fileName + "." + format));
    }
}
